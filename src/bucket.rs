pub struct Game{
	running: bool,
}

impl Game{
	pub fn new() -> Game {
		Game{
			running:false,
		}
	}

	pub fn run(&mut self){
		println!("in run");
		self.running = true;

		while self.running{
			println!("Game is running");
			self.running = false;
		}
		println!("stopping run");
	}
}